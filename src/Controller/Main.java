package Controller;

import java.util.List;
import java.util.Scanner;

import Entity.Tarjeta;
import Entity.Transaccion;
import business.OperacionTarjeta;
import business.Seguridad;

public class Main {

    public static void main (String args[]){
       Scanner reader = new Scanner (System.in);
       Tarjeta tarjeta = new OperacionTarjeta().nuevaTarjeta("Osvaldo", "Hernandez");
       Seguridad seg = new Seguridad();
       OperacionTarjeta opT = new OperacionTarjeta();
       int intentos = 0;
       boolean logged = false;
       while (tarjeta.status()) {
           if(intentos < 3) {
               String pin = null;
               if(!logged) {
                   System.out.println("-----------------------------------------------");
                   System.out.println("Digite su pin de acceso");
                   pin = reader.next();   
               }
               
               if(logged || seg.validaPin(pin, tarjeta)) {
                   if(!logged)
                       System.out.println("-------------------> BIENVENIDO "+tarjeta.getNombre()+" "+tarjeta.getApellido()+" <-------------------");
                   logged = true;
                   int opcion = opT.getOperacionesByTarjeta(tarjeta); 
                   if(opcion == 1) {
                       System.out.println("Saldo al momento: "+opT.consultarSaldo(tarjeta)+"\n");
                       continue;
                   }else if(opcion == 2) {
                       Tarjeta updateTarjeta = opT.retirarDinero(tarjeta);  
                       if (!tarjeta.getSaldo().equals(updateTarjeta.getSaldo())) {
                           tarjeta = updateTarjeta;
                       }
                       continue;
                   }else if (opcion == 3) {
                       List<Transaccion> lstTransaccion = tarjeta.getTransacciones();
                       for (Transaccion transaccion : lstTransaccion) {
                           System.out.println(".........");
                           System.out.println("Operacion realizada el: "+transaccion.getFecha());
                           System.out.println("Por la cantidad de: "+transaccion.getCantidad());
                           System.out.println("Teniendo un saldo al momento de: "+transaccion.getSaldo());
                           System.out.println(".........");
                       }
                       System.out.print("\n");
                       continue;
                   }else if(opcion == 0) {
                       break;
                   }else {
                       continue;
                   }
               }else {
                   intentos +=1;
               }
           }else {
               tarjeta.setStatus(false);
               System.out.println("Error, su tarjeta ha exedido el numero maximo de intentos, por su seguridad se ha cerrado la sesión");
           }
       }
       System.out.println("Vuelva pronto");
    }
}
