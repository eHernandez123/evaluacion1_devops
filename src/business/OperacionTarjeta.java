package business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import Entity.Tarjeta;
import Entity.Transaccion;

public class OperacionTarjeta {

    public Tarjeta nuevaTarjeta(String nombre, String apellido) {
        Tarjeta tarjeta = new Tarjeta();
        tarjeta.setNombre(nombre);
        tarjeta.setApellido(apellido);
        tarjeta.setNoTarjeta("XXXX - XXXX - XXXX - 7524");
        tarjeta.setPin("1235");
        tarjeta.setSaldo("1000");
        tarjeta.setStatus(true);
        tarjeta.setTransacciones(new ArrayList<Transaccion>());
        return tarjeta;
    }
    
    public int getOperacionesByTarjeta(Tarjeta tarjeta) {
        System.out.println("Seleccione el numero de opcion deseada: ");
        System.out.println("1. Consultar Saldo");
        System.out.println("2. Retirar Saldo");
        System.out.println("3. Historico de Movimientos");
        System.out.println("------------");
        System.out.println("0. Salir\n");
        try {
            Scanner reader = new Scanner (System.in);
            int opcion = reader.nextInt();
            return opcion;
        }catch(Exception e) {
            System.out.println("Solo puede ingresar números\n");
            return -1;
        }
    }
    
    public Tarjeta retirarDinero (Tarjeta tarjeta) {
        System.out.println("----------- Retiro de cuenta -----------");
        System.out.println("Ingrese la cantidad a retiar");
        try {
            Scanner reader = new Scanner (System.in);
            String cantidad = reader.next();
            BigDecimal result = (new BigDecimal(tarjeta.getSaldo()).setScale(2)).subtract(new BigDecimal(cantidad).setScale(2));
            if (result.compareTo(new BigDecimal(0)) < 0) {
                System.out.println( "No es posible realizar la operacion, saldo insuficiente \n");
            }else {
                String saldo = tarjeta.getSaldo();
                tarjeta.setSaldo(String.valueOf(result));
                Transaccion trn = new Transaccion();
                trn.setFecha(new Date());
                trn.setCantidad(cantidad);
                trn.setSaldo(saldo);
                tarjeta.getTransacciones().add(trn);
                System.out.println("Se han retirado "+cantidad+" de su cuenta \n");   
            }
        }catch(NumberFormatException e) {
            System.out.println("ERROR, Debe ingresar una cantidad valida");
        }
        return tarjeta;
    }
    
    public String consultarSaldo(Tarjeta tarjeta) {
        System.out.println("----------- Consulta de saldo -----------");
        return tarjeta.getSaldo();
    }
}
