package business;

import Entity.Tarjeta;

public class Seguridad {

    public boolean validaPin (String pin, Tarjeta tarjeta) {
        try {
            int pinC = Integer.parseInt(pin);
            if(pin != null && tarjeta != null) {
                if(tarjeta.getPin().equals(pin))
                    return true;
            }
            if(pin.length() > 4) {
                System.out.println("PIN incorrecto, no se permite un pin de mas de 4 digitos");
            }else if(pin.length() < 4) {
                System.out.println("PIN incorrecto, el pin esta incompleto");
            }else if (pin.length() == 4) {
                System.out.println("PIN incorrecto");
            }
        }catch(NumberFormatException e) {
            System.out.println("PIN incorrecto, el pin solo puede incluir números");
        }catch (NullPointerException e) {
            System.out.println("PIN incorrecto, debe ingresar un PIN");
        }
        return false;
    }
    
    
    public boolean validaMonto(double monto) {
        return false;
    }
}
