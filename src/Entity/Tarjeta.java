package Entity;

import java.util.List;

public class Tarjeta extends Cliente{
    
    private String noTarjeta;
    private String saldo;
    private String pin;
    private boolean status;
    private List<Transaccion> transacciones;
    
    public String getNoTarjeta() {
        return noTarjeta;
    }
    public void setNoTarjeta(String noTarjeta) {
        this.noTarjeta = noTarjeta;
    }
    public String getSaldo() {
        return saldo;
    }
    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }
    public String getPin() {
        return pin;
    }
    public void setPin(String pin) {
        this.pin = pin;
    }
    public boolean status() {
        return status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }
    public List<Transaccion> getTransacciones() {
        return transacciones;
    }
    public void setTransacciones(List<Transaccion> transacciones) {
        this.transacciones = transacciones;
    }
    
}
