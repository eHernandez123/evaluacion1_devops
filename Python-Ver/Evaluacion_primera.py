from decimal import Decimal, getcontext
from datetime import date
from datetime import datetime

def getObjTransaccion ():
    transaccion = {
        "Fecha":"",
        "Cantidad":"",
        "Saldo":""
    }
    return transaccion

def operacionesByTarjeta():
    print("Seleccione el numero de opcion deseada:")
    print("1. Consultar Saldo")
    print("2. Retirar Saldo")
    print("3. Historico de Movimientos")
    print("0. Salir")
    try :
        opcion = int(input("------------"))
        return opcion
    except ValueError:
        print("Solo puede ingresar números\n")
        return -1

def creaTarjeta ():
    tarjeta = {
        "noTarjeta":"123973246",
        "saldo":"1000",
        "pin":"1235",
        "status": True,
        "nombre":"Osvaldo",
        "apellido":"Hernandez",
        "transacciones":[]
    }
    return tarjeta

def validaPin(pin, tarjeta):
    try :
        pinC = int(pin)
        if pin is not None and tarjeta is not None:
            if tarjeta["pin"] == pin:
                return True
            if len(pin) > 4 :
                print("PIN incorrecto, no se permite un pin de mas de 4 digitos\n");
            elif len(pin) < 4:
                print("PIN incorrecto, el pin esta incompleto")
            elif len(pin) == 4:
                print("PIN incorrecto")
    except ValueError:
        print("PIN incorrecto, el pin solo puede incluir números")
    except Exception:
        print("PIN incorrecto, debe ingresar un PIN")
    return False

def retiraDinero (tarjeta):
    try:
        print("----------- Retiro de cuenta -----------\n")
        cantidad = input("Ingrese la cantidad a retiar\n")
        getcontext().prec = 6
        result = Decimal(float(tarjeta["saldo"])) - Decimal(float(cantidad))
        if result < 0:
            print("No es posible realizar la operacion, saldo insuficiente \n")
        else:
            saldo = tarjeta["saldo"]
            tarjeta["saldo"] = str(result)
            trs = getObjTransaccion()
            trs["Fecha"] = str(datetime.now())
            trs["Cantidad"] = cantidad
            trs["Saldo"] = str(saldo)
            tarjeta["transacciones"].append(trs)
            print(f"Se han retirado {cantidad} de su cuenta \n")
    except ValueError:
        print("Solo puede ingresar numeros")
    finally:
        return tarjeta


tarjeta = creaTarjeta()
intentos = 0
logged = False
while tarjeta["status"]:
    if intentos < 3:
        if not logged:
            print("-----------------------------------------------\n");
            pin = input("Digite su pin de acceso\n")
        if logged or validaPin(pin, tarjeta):
            if not logged:
                print(f"-------------------> BIENVENIDO {tarjeta['nombre']} {tarjeta['apellido']} <-------------------")
            logged = True
            opcion = operacionesByTarjeta()
            if opcion == 1:
                print(f"Saldo al momento: {tarjeta['saldo']}\n")
                continue
            if opcion == 2:
                updateTarjeta = retiraDinero(tarjeta)
                if not tarjeta["saldo"] == updateTarjeta["saldo"]:
                    tarjeta = updateTarjeta
                continue;
            if opcion == 3:
                lstTransaccion = tarjeta["transacciones"]
                for transaccion in lstTransaccion:
                    print(".........");
                    print(f"Operacion realizada ebl: {transaccion['Fecha']}")
                    print(f"Por la cantidad de: {transaccion['Cantidad']}")
                    print(f"Teniendo un saldo al momento de: {transaccion['Saldo']}")
                    print(".........\n");
            if opcion == 0:
                break
            else:
                continue

        else:
            intentos+=1
    else:
        tarjeta["status"] = False
        print("Error, su tarjeta ha exedido el numero maximo de intentos, por su seguridad se ha cerrado la sesión")